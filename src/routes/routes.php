<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'administration'], function () {
    Route::get('mail', [
        'as' => 'mail',
        'uses' => '\Avannubo\Mail\Controllers\MailController@index',
        'middleware' => ['permission:mail.view|mail.create|mail.edit']
    ]);
    Route::get('mail/add', [
        'as' => 'mail-add',
        'uses' => '\Avannubo\Mail\Controllers\MailController@create',
        'middleware' => ['permission:mail.create']
    ]);
    Route::get('mail/edit', [
        'as' => 'mail-edit',
        'uses' => '\Avannubo\Mail\Controllers\MailController@edit',
        'middleware' => ['permission:mail.edit']
    ]);
    Route::post('mail/add', [
        'as' => 'mail-add',
        'uses' => '\Avannubo\Mail\Controllers\MailController@store',
        'middleware' => ['permission:mail.create']
    ]);
    Route::put('mail', [
        'as' => 'mail-edit',
        'uses' => '\Avannubo\Mail\Controllers\MailController@update',
        'middleware' => ['permission:mail.edit']
    ]);
    
});