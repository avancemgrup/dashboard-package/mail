<?php

namespace Avannubo\Mail\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $fillable = [
        'id',
        'host',
        'port',
        'username',
        'password',
        'encryption',
        'address',
        'name',
    ];
}
