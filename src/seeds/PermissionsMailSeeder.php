<?php
namespace Avannubo\Mail\Seeds;
use Illuminate\Database\Seeder;
use App\Permission;
class PermissionsMailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $person = new Permission([
            'name' => 'mail.view',
            'display_name' => 'mail view',
            'description' => 'Ver mail'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'mail.create',
            'display_name' => 'mail create',
            'description' => 'Crear mail'
        ]);
        $person->save();

        $person = new Permission([
            'name' => 'mail.edit',
            'display_name' => 'mail edit',
            'description' => 'Editar mail'
        ]);
        $person->save();
    }
}
