<?php

namespace Avannubo\Mail;

use Illuminate\Support\ServiceProvider;

class MailServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //routes
        include __DIR__.'/routes/routes.php';
        //models
        include __DIR__.'/models/Mail.php';
        //view blade
        $this->loadViewsFrom(__DIR__.'/views', 'mail');
        //migration
        $this->loadMigrationsFrom(__DIR__.'/migrations');

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Avannubo\Mail\Controllers\MailController');
    }
}
