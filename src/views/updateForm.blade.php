@extends('layouts.administration.master')
@section('site-title')
    Mail
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Editar Mail</h2>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (Session::has('error'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ Session::get('error')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                {{ Session::get('message')  }}
                            </div>
                        </div>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'mail-edit', 'method'=>'PUT', 'autocomplete' => 'off')) !!}
                <div class="form-group {{ $errors->has('host') ? 'has-error' : '' }}">
                    <label for="host">Host</label>
                    {!! Form::text('host', $mail->host, array('placeholder' => 'Host','class' => 'form-control', 'id' => 'host')) !!}
                    <span class="text-danger">{{ $errors->first('host') }}</span>
                </div>
                <div class="form-group {{ $errors->has('port') ? 'has-error' : '' }}">
                    <label for="port">Puerto</label>
                    {!! Form::number('port', $mail->port, array('placeholder' => 'Port','class' => 'form-control', 'id' => 'port')) !!}
                    <span class="text-danger">{{ $errors->first('port') }}</span>
                </div>
                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                    <label for="username">Nombre de usuario</label>
                    {!! Form::text('username', $mail->username, array('placeholder' => 'Username','class' => 'form-control', 'id' => 'username')) !!}
                    <span class="text-danger">{{ $errors->first('username') }}</span>
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="password">Contraseña</label>
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control', 'id' => 'password')) !!}
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
                <div class="form-group {{ $errors->has('encryption') ? 'has-error' : '' }}">
                    <label for="encryption">Encriptación</label>
                    {!! Form::select('encryption', ['TLS' => 'TLS', 'SSL' => 'SSL', '' => 'NO'], $mail->encryption, ['id' => 'sel1', 'class' => 'form-control']) !!}
                    <span class="text-danger">{{ $errors->first('encryption') }}</span>
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address">Mail remitente</label>
                    {!! Form::email('address', $mail->address, array('placeholder' => 'Address','class' => 'form-control', 'id' => 'address')) !!}
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Nombre</label>
                    {!! Form::text('name', $mail->name, array('placeholder' => 'Name','class' => 'form-control', 'id' => 'name')) !!}
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
                @permission('mail.edit')
                <button type="submit" class="btn btn-success">
                   Actualizar
                </button>
                @endpermission
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
