@extends('layouts.administration.master')

@section('site-title')
    Mail
@endsection
@section('main-content')
    <div class="container-fluid">
        <div class="row card">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12 flex align-bottom">
                <div>
                    <h2>Crear nuevo Mail</h2>
                </div>
            </div>
        </div>
        <div class="row card">
            <div class="col-xs-12 col-md-12">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Hubo algunos problemas con tu entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {!! Form::open(array('route' => 'mail-add', 'method'=>'POST', 'autocomplete' => 'off')) !!}
                <div class="form-group {{ $errors->has('host') ? 'has-error' : '' }}">
                    <label for="host">Host</label>
                    {!! Form::text('host', null, array('placeholder' => 'Host','class' => 'form-control', 'id' => 'host')) !!}
                    <span class="text-danger">{{ $errors->first('host') }}</span>
                </div>
                <div class="form-group {{ $errors->has('port') ? 'has-error' : '' }}">
                    <label for="port">Puerto</label>
                    {!! Form::number('port', null, array('placeholder' => 'Puerto','class' => 'form-control', 'id' => 'port')) !!}
                    <span class="text-danger">{{ $errors->first('port') }}</span>
                </div>
                <div class="form-group {{ $errors->has('username') ? 'has-error' : '' }}">
                    <label for="username">Nombre</label>
                    {!! Form::text('username', null, array('placeholder' => 'Nombre de usuario','class' => 'form-control', 'id' => 'username')) !!}
                    <span class="text-danger">{{ $errors->first('username') }}</span>
                </div>
                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                    <label for="password">Contraseña</label>
                    {!! Form::password('password', array('placeholder' => 'Controseña','class' => 'form-control', 'id' => 'password')) !!}
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
                <div class="form-group {{ $errors->has('encryption') ? 'has-error' : '' }}">
                    <label for="encryption">Encriptación</label>
                    {!! Form::select('encryption', ['TLS' => 'TLS', 'SSL' => 'SSL', 'NO' => 'NO'], null, ['id' => 'sel1', 'class' => 'form-control']) !!}
                    <span class="text-danger">{{ $errors->first('encryption') }}</span>
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                    <label for="address">Mail remitente</label>
                    {!! Form::email('address', null, array('placeholder' => 'Dirección','class' => 'form-control', 'id' => 'address')) !!}
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">Nombre</label>
                    {!! Form::text('name', null, array('placeholder' => 'Nombre','class' => 'form-control', 'id' => 'name')) !!}
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
                @permission('mail.create')
                <button type="submit" class="btn btn-success">
                   Crear
                </button>
                @endpermission
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection