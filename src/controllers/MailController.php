<?php

namespace Avannubo\Mail\Controllers;

use App\Http\Controllers\Controller;
use Avannubo\Mail\Models\Mail;
use Illuminate\Http\Request;
use Session;
class MailController extends Controller{

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @description This method show the index view
     */
    public function  index(){
        $webMail = Mail::first();
        if ($webMail) {
            return $this->edit();
        } else {
            return $this->create();
        }
//        return view('mail::index', compact('webMail'));
    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @description: This method validates the existence of the information in the database,
     * if it exists it redirects a web-mail else show the form for insert the information
     */
    public function create(){
        if(Mail::first()){
            return redirect('mail');
        }
        return view('mail::form');
    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it inserts it into the database
     * else Shows error
     *
     */
    public function store(Request $request){
        if( Mail::first()){
            return redirect()->route('mail')->with('error','Not found result');
        }
        // Validation
        $rules = [
            'host' => 'required|Max:255',
            'port' => 'required|integer',
            'username' => 'required|Max:255',
            'password' => 'required|Max:100',
            'encryption' => 'Max:5',
            'address' => 'required|email|Max:255',
            'name' => 'required',
        ];

        $this->validate($request, $rules);

        $env_file = base_path('.env');

        // Host
        $url = $request->input('host');
        if (!preg_match('#^http(s)?://#', $url)) {
            $url = 'http://' . $url;
        }
        $url = parse_url($url);

        // Encryption
        if ($request->input('encryption') != "TLS" && $request->input('encryption') != "SSL") {
            $request['encryption'] = "";
        }

        if (file_exists($env_file)) {
            $this->updateEnv($env_file, 'mail.host', 'MAIL_HOST', $url['host']);
            $this->updateEnv($env_file, 'mail.port', 'MAIL_PORT', $request->input('port'));
            $this->updateEnv($env_file, 'mail.username', 'MAIL_USERNAME', $request->input('username'));
            $this->updateEnv($env_file, 'mail.password', 'MAIL_PASSWORD', $request->input('password'));
            $this->updateEnv($env_file, 'mail.encryption', 'MAIL_ENCRYPTION', $request->input('encryption'));
            $this->updateEnv($env_file, 'mail.from.address', 'MAIL_FROM_ADDRESS', $request->input('address'));
            $this->updateEnv($env_file, 'mail.from.name', 'MAIL_FROM_NAME', $request->input('name'));
        }

        ///Add information
        $mail = new Mail([
            'host' => $url['host'],
            'port' => $request->input('port'),
            'username' => $request->input('username'),
            'password' => $request->input('password'),
            'encryption' => $request->input('encryption'),
            'address' => $request->input('address'),
            'name' => $request->input('name'),
        ]);

        if($mail->save()){
            return redirect()->route('mail')->with(
                'message','Mail creado correctamente'
            );
        }else{
            return redirect()->route('mail')->with(
                'error','Mail no creado correctamente,intentelo mas tarde'
            );
        }

    }


    /**
    * @author: Obiang
    * @date: 27/06/2017
    * @param $id
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    * @description This method validates the existence of the information in the database,
    * if it exists it redirects a web-mail else show the form for update the information
    */
    public function edit(){
        $mail = Mail::first();
        if($mail){
            return view('mail::updateForm',compact('mail'));
        }
        return redirect()->route('mail')->with('error','Mail no encontrado');
    }

    /**
     * @author: Obiang
     * @date: 27/06/2017
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @description This method validates the values entered by the client, if they are correct it update it into the database
     * else Shows error
     */
    public function update(Request $request){
        // Validation
        $mail = Mail::first();

        if($mail){
            $rules = [
                'host' => 'required|Max:255',
                'port' => 'required|integer',
                'username' => 'required|Max:255',
                'password' => 'Max:100',
                'encryption' => 'Max:5',
                'address' => 'required|email|Max:255',
                'name' => 'required',
            ];

            $this->validate($request, $rules);
            $env_file = base_path('.env');

            if($request->input('host')){
                $url = $request->input('host');
                if (!preg_match('#^http(s)?://#', $url)) {
                    $url = 'http://' . $url;
                }
                $url = parse_url($url);
                $mail->host = $url['host'];
                if (file_exists($env_file)) $this->updateEnv($env_file, 'mail.host', 'MAIL_HOST', $url['host']);
            }
            if($request->input('port')){
                $mail->port = $request->input('port');
                if (file_exists($env_file)) $this->updateEnv($env_file, 'mail.port', 'MAIL_PORT', $request->input('port'));
            }
            if($request->input('username')){
                $mail->username = $request->input('username');
                if (file_exists($env_file)) $this->updateEnv($env_file, 'mail.username', 'MAIL_USERNAME', $request->input('username'));
            }
            if($request->input('password')){
                $mail->password = $request->input('password');
                if (file_exists($env_file)) $this->updateEnv($env_file, 'mail.password', 'MAIL_PASSWORD', $request->input('password'));
            }
            // Encryption
            if ($request->input('encryption') != "TLS" && $request->input('encryption') != "SSL") {
                $request['encryption'] = "";
            }
            $mail->encryption = $request->input('encryption') == null ? "" : $request->input('encryption');
            if (file_exists($env_file)) {
                $this->updateEnv($env_file, 'mail.encryption', 'MAIL_ENCRYPTION', $request->input('encryption'));
            }
            // Finish Encryption
            if($request->input('address')){
                $mail->address = $request->input('address');
                if (file_exists($env_file)) $this->updateEnv($env_file, 'mail.from.address', 'MAIL_FROM_ADDRESS', $request->input('address'));
            }
            if($request->input('name')){
                $mail->name = $request->input('name');
                if (file_exists($env_file)) $this->updateEnv($env_file, 'mail.from.name', 'MAIL_FROM_NAME', $request->input('name'));
            }

            if( $mail->update()){
                return redirect()->route('mail')->with(
                    'message', 'Mail actualizado correctamente'
                );
            }else{
                return redirect()->route('mail')->with(
                    'error', 'Mail no actualizado correctamente,intentelo mas tarde'
                );
            }

        }
        return redirect()->route('mail')->with('error', 'Mail no encontrado');
    }

    private function updateEnv($env_file, $config, $key, $value) {
        file_put_contents($env_file,
            str_replace(
                $key . '="' . config($config) . '"',
                $key . '="' . $value . '"',
                file_get_contents($env_file)
            )
        );
    }
}
