## Avannubo package mail

 Mail

## Install

* Config composer.json: 
    
    require: {
    
        ...
        
        "avannubo/mail": "dev-master"
    }
   
    "repositories": [
        
        ...
        
        {
            
                "type": "vcs",
                
                "https://gitlab.com/avancemgrup/dashboard-package/mail.git"
                
        }
            
    ]

* Install: `composer update`
* Add to Service Provider: `Avannubo\Mail\MailServiceProvider::class`
* seeders: `php artisan db:seed --class=Avannubo\Mail\Seeds\PermissionMailSeeder`
